$(function(){
          $("[data-toggle='tooltip']").tooltip();
          $("[data-toggle='popover']").popover();
          $('.carousel').carousel({
            interval:2000
          });

          $('#contacto').on('show.bs.modal', function (e){
              console.log('el modal contacto se esta mostrando');

              $('#verBtn').removeClass('btn-outline-success');
              $('#verBtn').addClass('btn-primary');
              $('#verBtn').prop('disabled', true);
          });

          $('#contacto').on('shown.bs.modal', function (e){
              console.log('el modal contacto se ha mostrado');
          });

          $('#contacto').on('hide.bs.modal', function (e){
              console.log('el modal contacto se esta ocultando');
          });

          $('#contacto').on('hidden.bs.modal', function (e){
              console.log('el modal contacto se ha ocultado');

                $('#verBtn').prop('disabled', false);
          });

      });